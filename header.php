<!DOCTYPE html>
<html>
<head>
  <title>My Rug Design</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <!-- fonts -->
  <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <!-- slider swiper -->
  <link rel="stylesheet" href="assets/css/swiper.min.css" type="text/css" />
  <link rel="stylesheet" type="text/css" href="assets/css/xzoom.css" media="all" />
  <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
<!--   <section class="top-header">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12">
          <div class="logo text-center">
            <img src="assets/images/logo.png">
          </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12">
          <div class="top-link-wrapper">
            <div class="top-social-link">
              <span class="mr-3"> Follow Us:</span>
              <span class="social-icon"><a href=""><i class="fab fa-facebook-f"></i></a></span>
              <span class="social-icon"><a href=""><i class="fab fa-twitter"></i></a></span>
              <span class="social-icon"><a href=""><i class="fab fa-google-plus-g"></i></i></a></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> -->
  <section class="middle-bar">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
           <nav class="navbar navbar-expand-lg navbar-light custom-navbar">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
             <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href="index.php">HOME</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="single-page.php">DESIGNER COLLECTION</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="single-page.php">CLASSIC</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="single-page.php">ELEMENTRY</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="single-page.php">CUSTOM RUG GUIDE</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="single-page.php">ABOUT US</a>
                </li>
              </ul>
            </div>
          </nav>
      </div>
    </div>
  </section>
<!-- <section id="top-menu">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <nav class="navbar navbar-expand-lg navbar-light custom-navbar">
          <a class="navbar-brand" href="#"><img src="assets/images/logo.png" class="logo" alt=""></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
           <ul class="navbar-nav">
              <li class="nav-item active">
                <a class="nav-link" href="index.php">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">About Us</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Blog</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Gallery</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Contact Us</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  </div>
</section> -->