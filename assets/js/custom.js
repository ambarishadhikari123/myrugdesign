(function ($) {
    $(document).ready(function() {
        $('.xzoom, .xzoom-gallery').xzoom({zoomWidth: 400, title: true, tint: '#333', smoothZoomMove:12, Xoffset: 15});
    });
})(jQuery);
$(document).ready(function(){
  /*main slider top*/
  var swiper = new Swiper('.main-slider', {
      slidesPerView: 1,
      loop: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      autoplay: {
         delay: 5000,
       },
    });
});
